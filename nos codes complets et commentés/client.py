#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 12 09:30:17 2020

@authors: Jiuaxan,Pauline,Darina,Rim
"""

from message import Message #importe la class message
from messageCatalogue import MessageCatalog #importe la classe message catalog
import requests #importe le module requests

class Client:
    def __init__(self, catalog, debug = False): #initialise la classe client
        self.messages=catalog #la classe client est composée d'un catalogue appelé self.messages
        
    def say_something(self, host): #fonction qui renvoie le statut d'une requete sur le serveur host
        m = Message() #initialisation d'un message avec les paramètres par défaut
        r = requests.post(host+"/"+m.msg) #envoie une requête sur le serveur host 
        if r.status_code == 201: #si le statut de la requete est 201, la requête est traitée avec succès et une ressource a été créée en conséquence
            print("Super, la connexion a bien été établie") #super, la connexion au serveur a bien été effectuée
            self.messages.add_message(m) #ajoute le message au catalogue du client
        else: #sinon
            print("Dommage, la connexion n'a pas fonctionné. Merci de réessayer.") #ça craint, la connexion au serveur n'a pas fonctionné
        return r.status_code #renvoie le statut de la requete
            
    def get_covid(self, host):
        response = requests.get(host) # on demande à au serveur de nous fournir la liste des messages des malades
        self.messages.c_import(response) # On intègre la liste messages envoyées par l'hopital au catalogue du client
        self.messages.quarantine(3) # on verifieà partir de ce nouveau catalogue si le client doit se mettre en quarantaine ou non 
    
    def send_history(self, host):
        self.messages.purge(14,Message.MSG_ISAID) # on se debarrase avant des messages qui datent de plus de 14 jours
        r = requests.post(host,self.messages.c_export_type_json(Message.MSG_ISAID)) #On envoit au serveur web (ici l'hopital) nos messages ISAID car on a eu le covid
        return r.status_code == 201 #renvoit FALSE si la requête n'a pas été bien prise en compte

if __name__ == "__main__":
    c = Client(MessageCatalog()) #création d'un nouveau client
    c.say_something("http://localhost:5000") #renvoie si la connexion est bien effectuée ou non
    c.get_covid("http://localhost:5000/they-said")  #demande à acceder aux informations contenus dans la page they-said pour récupérer la liste des messages des personnes malades
    c.send_history("http://localhost:5000/they-said") #demander à acceder à la page they-said pour envoyer les messages du client ISAID car ce dernier est positive aux covids.
