#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov  8 19:28:20 2020

@authors: Jiuaxan,Pauline,Darina,Rim
"""
import json
from message import Message
import time

class MessageCatalog: #Création de la classe message catalogue
    def __init__(self): #Initialisation de la classe
        self.file ='client.json' #'client.json' #file est le fichier s'appelant client.json
        self.messages = []#Les messages sont dans une liste self.messages
    
    def c_import(self, content):#Import object content (can import batch of THEY SAID or other for testing purposes)
        for i in content: #pour chaque message dans la liste content  mise en paramètre
            if  self.check_msg(i.msg,Message.MSG_THEY) == False: #si le message n'est pas  déjà dans la liste content    (pour éviter les redondances)
                self.messages.append(i) #alors on peut l'ajouter
        self.mise_a_jour() #on met à jour la liste des messages dans le catalogue client dans le fichier json
    
    def c_export_type(self,msg_type=Message.MSG_ISAID): #export the messages of a given type from a catalog
        res = [] #on initialise le resultat comme une liste vide
        for i in self.messages: #pour chaque message le catalogue self.messages
            if i.type == msg_type: #si le type du message est le type en paramètre
                res.append(i) #alors on l'ajoute dans la liste resultat
        return res #renvoie la liste des messages que j'ai dit
    
    def c_export_type_json(self,msg_type=Message.MSG_ISAID): #exporter les messages du type donnée en format json
        res = [] #on initialise le resultat comme une liste vide
        for i in self.messages: #pour chaque message le catalogue self.messages
            if i.type == msg_type: #si le type du message est le type en paramètre
                res.append(i.dict_msg) #alors on l'ajoute dans la liste le str du type json associé avec ce message
        return res #renvoie la liste des messages que j'ai dit
    
    def mise_a_jour(self): #fonction de mise à jour de la liste des messages du catalogue client dans le fichier json
        data=[]#on crée une liste vide data 
        for i in self.messages:#pour chaque message dans le catalogue self.messages
            data.append(i.dict_msg) #on ajoute le message dans la liste data sous format de chaine JSOON grâce à dict_msg 
        with open(self.file,'w') as f: #on ouvre le fichier en ecriture
            json.dump(data,f) #on écrit les données sérialisées dans le fichier json 'client'json' 
    
    def add_message(self,m,save=True): #on ajoute un message au catalogue et on fait une mise à jour si save=True
        self.messages.append(m) #on ajoute le message m
        if save == True: #si true on demande une mise à jour
            self.mise_a_jour() #on met alors à jour le catalogue client  dans le fichier json
    
    #remove all messages of msg_type that are older than max_age days
    #msg_type is false for deleting all messages of all types older than max_age
    def purge(self,max_age=14,msg_type=False): #Fonction qui enlève les messages qui sont trop vieux (age supérieur à max age , ici 14 s)
        rem = [] #initialisation de rem à une liste vide
        if msg_type == False: #si le msg_type est False (par défaut)
            for i in self.messages: #on parcourt les messages du catalogue
                if i.age(False)>max_age+0.1: #si age est plus grand que le max de l'age +0.1 de compilation
                    rem.append(i) #alors on l'ajoute à rem
        else:#sinon
            for i in self.messages: #on parcourt les messages du catalogue
                if i.age(False)>max_age+0.1 and i.type == msg_type: #Si l'age du message est plus grand que l'age maximun en paramètre ET le type de message est celui en paramètre (autre que False)
                    rem.append(i)#alors on l'ajoute à rem
        for i in rem:#pour toutes les valeurs de rem
            self.messages.remove(i) #on les enlève du catalogue
        self.mise_a_jour() #on met alors à jour le catalogue client dans le fichier json

    #Check if message string is in a category
    # 	def check_msg(self, msg_str, msg_type=Message.MSG_THEY):
    def check_msg(self,msg_str,msg_type=Message.MSG_IHEARD): #Fonction qui renvoie si un message est du type MSG_IHEARD 
        msgs = self.c_export_type(msg_type) #on exporte les messages du type "MSG_IHEARD" j'ai entendu et on les  ajoute dans liste msgs
        res = False #res est initialisé à False
        for i in msgs: #pour chaque message dans msgs
            if i.msg==msg_str: #si le message msg_str en paramètre est le même que le message dans msgs du type MSG_IHEARD
                res = True #alors res devient True, ce msg_str est de type MSG_IHEARD
        return res #renvoie res
    
    #Say if I should quarantine based on the state of my catalog
    def quarantine(self,nb_heard=3):#Fonction qui dit si on doit se mettre en quarantaine ou non
        nb = 0 #on initialise nb à 0
        msgThey = self.c_export_type(Message.MSG_THEY) #affecte à msgThey les messages de type THEY exportés des malades 
        for i in msgThey: #pour chaque message de msgThey (liste des messages des malades)
            if self.check_msg(i.msg)==True: #si le message est dans la liste des messages que j'ai entendus
                nb = nb +1 #alors on incrémente nb
        if nb >= nb_heard: #si ce nombre est plus grand de nb_heard en paramètre (par défaut 3)
            return True #alors dire oui (on doit se confiner)
        else:
            return False #sinon il n'est pas nécessaire de se confiner
    
    def c_export(self):#Fonction qui exportepour afficher sur python les données
        temp = ''#initialisation de temp comme str vide
        with open(self.file) as f: #ouvrir le fichier
             pop_data = json.load(f) #le charger en Python
             for pop_dict in pop_data: #on parcourt les données
                  msg = pop_dict['message'] #msg est la valeur de la clé message du dictionnaire pop_dict
                  typem = pop_dict['type'] #typem est la valeur de la clé type du dictionnaire pop_dict
                  date = pop_dict['date'] #date est la valeur de la clé date du dictionnaire pop_dict
                  temp = temp + str(typem) + ' ' + str(date) + ' ' + str(msg) #temp est une chaine de caractères qui concatène tous ces éléments
                  temp = temp + '\n' #on retourne à la ligne
             # temp=pop_data  #on aurait égelement pû utiliser le format sous lequel sont renvoyés les données par load (dictionnaire python)
        if temp == '': #si temp est toujours vide
            return 'vide' #on renvoie vide
        else:#sinon
            return temp #on renvoie la valeur de temp

        


# #will only execute if this file is run
# if __name__ == "__main__":
    #test the class
    # catalog = MessageCatalog("test.json")#on initialise un nouveau catalogue catalog avec la file "test.json"
    # catalog.add_message(Message())#on ajoute un message avec les paramètres par défaut
    # catalog.add_message(Message(Message().generate(), Message.MSG_THEY))#on ajoute un autre paramètre de ils ont dit avec un message généré
    # print(catalog.c_export())# on exporte le catalogue pour l'afficher sous python
    # time.sleep(0.5)#on attend 1/2 seconde
    # catalog.add_message(Message(Message().generate(),Message.MSG_IHEARD))#on ajoute un message de type  j'ai entendu au catalogue
    # catalog.add_message(Message())#on ajoute un autre message avec les paramètres par défaut
    # print(catalog.c_export())#on exporte le catalogue
    # time.sleep(0.5)#on attend 1/2 seconde
    # catalog.add_message(Message())#on ajoute un autre message avec les paramètres par défaut
    # print(catalog.c_export())#on exporte le catalogue
    # time.sleep(2)#on attend 2 secondes
    # catalog.purge(2)#on enlève tout les messages qui ont plus de 2 secondes (soit 2 jours dans notre problème)
    # print(catalog.c_export())#on exporte le catalogue




# Mc= MessageCatalog("test.json")
# m1 = Message()
# m2 = Message()
# m21 = m2.export_IHEARD()
# m22 = m2.export_THEY()
# m3 = Message()
# m31 = m3.export_THEY()
# m32 = m3.export_IHEARD()
# m4 = Message()
# M1 = [m1,m21,m22,m32,m31]
# Mc.c_import(M1)
# M3 = Mc.c_export_type(Message.MSG_ISAID)
# Mc.c_import(M3)
# Mc.add_message(m4)
# msgs = Mc.c_export_type(Message.MSG_IHEARD)
# Mc1= MessageCatalog("test1.json")
# Mc1.c_import(msgs)
# print(Mc.check_msg(m21.msg))
# print(Mc.quarantine(2))
# time.sleep(2)
# Mc.purge(2,Message.MSG_IHEARD)