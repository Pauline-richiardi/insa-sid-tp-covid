#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov  8 19:29:06 2020

@authors: Jiuaxan,Pauline,Darina,Rim
"""

#importation des librairies nécessaires
import json
import random
import string 
import time

class Message:
    MSG_THEY='THEY' #Message que les malades ont dit 
    MSG_IHEARD='IHEARD' #Message que j'entends
    MSG_ISAID='ISAID' #Message que je dis
    
    def __init__(self, msg='',msg_type=False, msg_date=False):#Fonction qui initialise la classe
        
        if msg == '': #Si le message est vide
            self.msg = self.generate() #on en génère un aléatoire 
        else: #sinon
            self.msg = msg #Le message est celui qui est donné
        if msg_type==False: #Si le type de message est Faux par défaut
            self.type=self.MSG_ISAID #alors c'est un message que je dis
        elif msg_type==self.MSG_IHEARD: #Si c'est un message que j'entends
            self.type=self.MSG_IHEARD #alors je le définis comme tel
        elif msg_type==self.MSG_THEY: #si c'est un message que les malades ont dit 
            self.type=self.MSG_THEY #alors je le définis comme tel
        if msg_date == False: #si la date du message est False par défaut
            self.date = time.time() #alors je définis la date du message  avec la date actuelle
        else: #sinon
            self.date = msg_date #la date est la date donnée en paramètre
        self.dict_msg={} #dictionnaire message pour json
        self.dict_msg['type']=self.type #on ajoute à la clé 'type' la valeur self.type
        self.dict_msg['date']=self.date #on ajoute à la clé 'date' la valeur self.date
        self.dict_msg['message']=self.msg #on ajoute à la clé 'message' la valeur self.msg
    
    def age(self,days=True): #Fonction qui calcule l'age soit en secondes ou en jours si days=True
        if days == True: #Si days = True (par défaut)
            return (time.time()-self.date)/86400 #Alors on met l'age en jours (en divisant par le nombre de secondes correspondant à un jour) en faisant la différence entre maintenant et sa date
        else: #sinon
            return time.time()-self.date #on met l'age en secondes en faisant la différence entre maintenant et sa date
    
    def generate(self): #on génère un message aléatoire  quelconque
        return ''.join(random.choice(string.ascii_uppercase+string.digits) for i in range(10))#on génère un message quelconque de 10 caractères consitué des lettres et des chiffres
    
    def __str__(self): #Fonction qui met notre dictionnaire message sous forme de chaine json
        return json.dumps(self.dict_msg)
    
    def m_export(self): #Fonction qui renvoie le message
        m = self.msg #le message est self.msg
        return m
    
    def m_import(self, Msg): #fonction qui nous dit si notre message correspond au Msg que l'on souhaite importer
        if self.msg == Msg: #si self.msg est égal à Msg en paramètre
            return True #alors oui
        else:
            return False#sinon non
    
    def export_IHEARD(self): #fonction qui exporte un message et déclare son type en IHEARD "j'ai entendu"
        m = self.msg
        return Message(m,Message.MSG_IHEARD,self.date) #création d'un message qui a le meme contenu et la meme date mais le type IHEARD différent
    
    def export_THEY(self): #fonction qui exporte un message et déclare son type en THEY "les malades ont dit" 
        if self.type==Message.MSG_ISAID:#si c'est un message que j'ai dit
            return Message(self.msg,Message.MSG_THEY,self.date) #alors on crée un message de type THEY "les malades ont dit" mais avec la même date et le même message que self
        else: #sinon
            return Message('Erreur du type:ISAID-THEY',0,self.date) #création de message avec le message 'Erreur du type:ISAID-THEY', le type 0 et self.date

# m = Message()
# print(m.msg,m.type,m.date,m.age())
# print(m.dict_msg)
# time.sleep(1)
# print(m.age())
# print(m)
# print(m.m_export())
# print(m.m_import('123'))
# print(m.export_THEY())


#will only execute if this file is run
if __name__ == "__main__":
    myMessage = Message()#nouveau message myMessage avec un msg vide, un type false et un date false--> il s'agit donc d'un message de type ISAID 
    time.sleep(1)#attendre 1 seconde (représentant 1 jour dans notre problème)
    mySecondMessage = Message(Message().generate(), Message.MSG_THEY)#Création d'un second message qu'on génère du type ils ont dit 
    copyOfM = Message(myMessage.m_export())#on copie le message  et on en crée un nouveau avec le même message et type que MyMessage mais avec une date superieur 
    print(myMessage)#on retourne myMessage
    print(mySecondMessage)#on retourne mySecondMessage
    print(copyOfM)#on retourne copy0fM
    time.sleep(0.5)#attendre 1/2 seconde
    print(copyOfM.age())#Retourner l'age de copy0fM en jours
    time.sleep(0.5) #attendre 1/2 seconde
    print(copyOfM.age(False)) #Retourner l'age en seconde

