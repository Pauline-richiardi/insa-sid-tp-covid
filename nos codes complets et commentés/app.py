#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 12 09:35:58 2020

@authors: Jiuaxan,Pauline,Darina,Rim
"""

from flask import Flask #importe flask
from flask import request #importe request
from flask import Response #importe response
import json #importe json
from message_catalog import MessageCatalog #importe message catalog
from message import Message #importe message
from client import Client

app=Flask(__name__) #le pamaètre est  le nom donné à l’applicationqui vaut l’attribut prédéfini [__name__] qui vaut [__main__] 

@app.route('/')
def index(): #Il s'agit de la fonction qui retourne un message  lorsqu’on demande l’URL [/] à l’application
    return "Bonjour, tout le monde ! Tout va très bien se passer."

@app.route('/<msg>', methods=['POST']) #la méthode POST est la seule autorisée dans @app.route
def add_heard(msg): #Fonction qui retourne la reponse du serveur
    print("add catalog "+msg) #affiche add catalog et le contenu du message posté par le client
    return Response("Le message a bien été entendu",status=201) #renvoie la réponse du serveur

@app.route('/they-said', methods=['GET','POST'])
def hospital():
     if request.method == 'GET': #si le client envoie une requête pour avoir la liste des malades
         client.messages.purge(14) #on supprime les données obsolètes du client
         return Response(client.messages.c_export_type(Message.MSG_THEY), status=201) #le serveur envoie la liste des messages des malades
     elif request.method == 'POST': #si le client envoie une requête pour informer l'hôpital qu'il est malade et qu'il envoie ce qu'il a dit
         client_said = json.loads(request.get_json()) #importe les données json envoyées par le client (str) en Python
         return Response(client.messages.c_import(client_said), status=201) #le serveur importe ce que le client malade a dit

if __name__ == "__main__":
    catalog = MessageCatalog() #crée un catalogue avec les paramètres par défaut
    client = Client(MessageCatalog())
    app.run(host="0.0.0.0", debug=True) # avec app run on lance le serveur et avec debug=False , il ne va pas afficher l'aide au débogage en cas d'erreur
