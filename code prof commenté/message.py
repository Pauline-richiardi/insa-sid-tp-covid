#!/usr/bin/env python3
import json
#import time
#replaced by the following two lines for testing purposes
import falsifytime
time = falsifytime.falsifytime() #affecte à la variable de temps le temps fictif actuel en secondes

#for generation of messages
from random import random
from hashlib import sha256

class Message:
	#some constants (message types)
	MSG_ISAID = 0 	#What-I-said message
	MSG_IHEARD = 1	#What-I-heard message
	MSG_THEY = 2	#What-covid-19-infected-said message
	#the constructor can take various parameters
	#default is parameterless, it generates a message to be said
	#if type is not specified, it is to be imported from an export string or parsed export string
	#otherwise, if date is False the time is system time
		#else it is a float, then the date should be this parameter
	def __init__(self, msg="", msg_type=False, msg_date=False):
		if msg == "": #si le message initial est vide, on le crée
			self.content = Message.generate() #contenu du message aléatoire généré
			self.type = Message.MSG_ISAID #un message est décrit initialement comme un message ISAID
			self.date = time.time() #date propre au message (date fictive d'envoi actuelle ou de réception actuelle en secondes)
		elif msg_type is False : #initalement, le type de message n'est pas défini
			self.m_import(msg) #on importe le message en format de chaine de caractères pour l'exploiter en json.
		else:
			self.content = msg #si le message est décrit, on récupère son contenu
			self.type = msg_type #si le message est décrit, on récupère son type
			if msg_date is False: #si la date n'est pas précisée, on affecte au nouveau message la date actuelle fictive
				self.date = time.time()
			else:
				self.date = msg_date #si le message est décrit, on récupère sa date

	#a method to print the date in a human readable form
	def hdate(self):
		return time.ctime(self.date)
    #renvoie la date réelle convertie au format standard compris 'Thu, 28 Jun 2001 14:17:15 +0000'

	#a method to compute the age of a message in days or in seconds
	#for display purpose, set as_string to True
	def age(self, days=True, as_string=False):
		age = time.time() - self.date # (secondes) temps de référence initial - date fictive actuelle du message
		if days:
			age = int(age/(3600*24)) #conversion en jours
		if as_string:
			d = int(age/(3600*24)) #conversion en jours
			r = age%(3600*24) #reste non entier de jours
			h = int(r/3600) #conversion en heures du reste de jours non entiers
			r = r%3600 #reste des heures non entières
			m = int(r/60) #conversion du reste des heures non entières en minutes
			s = r%60 #conversion du reste en secondes
			age = (str(age)+"~"+str(d)+"d"+ str(h) + "h"+str(m)+"m"+str(s)+"s")
		return age #retourne l'âge du message en format chaine de caractères en jours, heures, minutes, secondes

	#testers of message type
	def is_i_said(self): #teste si le type du message est un "ISAID"
		return self.type == Message.MSG_ISAID #renvoie un booléen
	def is_i_heard(self): #teste si le type du message est un "IHEARD"
		return self.type == Message.MSG_IHEARD #renvoie un booléen
	def is_they_said(self): #teste si le type du message est un "THEY"
		return self.type == Message.MSG_THEY #renvoie un booléen

	#setters
	def set_type(self, new_type): #change le type du message
		self.type = new_type

	#a class method that generates a random message, ne dépend pas, peut-être appelée en dehors d'une instance classe
	@classmethod
	def generate(cls): #génère un message aléatoire
		return sha256(bytes(str(time.time())+str(random()),'utf8')).hexdigest()

	#a method to convert the object to string data
	def __str__(self): #transforme un objet (message) Python en chaine de caractères
		return f"\t\t<message type=\"{self.type}\">\n\t\t\t<content>{self.content}</content>\n\t\t\t<date>{self.hdate()}</date>\n\t\t</message>"

	#export/import
	def m_export(self): #exporte le message Python en chaînes de caractères exploitable sous json
		return f"{{\"content\":\"{self.content}\",\"type\":{self.type}, \"date\":{self.date}}}"

	def m_import(self, msg): #importe les données selon sa source (client ou serveur)
		if(type(msg) == type(str())): #si le message est une chaine de caractères
			json_object = json.loads(msg) #convertit le message en objet python
		elif(type(msg) == type(dict())): #si le message est un objet Python
			json_object = msg #convertit l'objet Python en chaine de caractères exploitables en json
		else:
			raise ValueError #si aucun des 2 formats n'est reconnu
		self.content = json_object["content"] #convertit le contenu du message importé en json
		self.type = json_object["type"] #convertit le type du message importé en json
		self.date = json_object["date"] #convertit la date du message importé en json

#will only execute if this file is run
if __name__ == "__main__":
	#test the class
	myMessage = Message() #crée un message ISAID
	time.sleep(1) #laisse s'écouler une seconde
	mySecondMessage = Message(Message.generate(), Message.MSG_THEY) #on crée un deuxième message aléatoire, de type THEY, message d'un malade
	copyOfM = Message(myMessage.m_export()) #exporte une copie du msg ISAID dans laquelle est exporté le message Python en chaîne de caractères
	print(myMessage) #affiche mon message de type ISAID
	print(mySecondMessage) #affiche mon 2e message de type THEY
	print(copyOfM) #affiche une copie du message de base, il sera amené à évoluer avec le temps
	time.sleep(0.5) #laisse s'écouler une demie seconde
	print(copyOfM.age(True,True)) #affiche l'age du message copié en jours
	time.sleep(0.5) #laisse s'écouler une demie seconde
	print(copyOfM.age(False,True)) #affiche l'age du message copié en format jours, heures, minutes, secondes
