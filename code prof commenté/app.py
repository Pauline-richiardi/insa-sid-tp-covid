#!/usr/bin/env python3
from flask import Flask #server
from flask import request #to handle the different http requests
from flask import Response #to reply (we could use jsonify as well but we handled it)
from flask import render_template #to use external files for responses (out of the scope of the project, added to provide a test UI)
import json
#My libraries
from message import Message
from client import Client #embarks message_catalog

app = Flask(__name__)

#“Routes” will handle all requests to a specific resource indicated in the
#@app.route() decorator

#These route are for the "person" use case
	#case 1 refuse GET
@app.route('/', methods=['GET'])
def index(): #fonction de l'hôpital qui refuse l'accès à la liste des patients malades à un client 
	response = render_template("menu.html",root=request.url_root, h="")
	return response 

	#case 2 hear message
@app.route('/<msg>', methods=['POST'])
def add_heard(msg): #permet d'envoyer au serveur(autre individu) un message que j'ai dit (ISAID->IHEARD)
	if client.catalog.add_message(Message(msg, Message.MSG_IHEARD)): #si le client a effectivement ajouté un message entendu à son catalogue
		response = Response(f"Message “{msg}” received.", mimetype='text/plain', status=201) #le serveur va "entendre le message" et affiche que le message a bien été reçu
	else : #sinon
		reponse = Response(status=400) #La requête n'a pas été trouvée
	return response #affiche une réponse ou une erreur.
#End of person use case

#Hospital use case
@app.route('/they-said', methods=['GET','POST']) #méthodes utilisées par le client
def hospital():
	if request.method == 'GET': #si la requête reçue par le serveur est un GET, il s'agit d'une demande d'un client de la liste THEY SAID
		#Wants the list of covid messages (they said)
		client.catalog.purge(Message.MSG_THEY) #mise à jour de la liste des messages de type THEYSAID du catalogue du client en question
		response = Response(client.catalog.c_export_type(Message.MSG_THEY), mimetype='application/json', status=200) #renvoie la liste des messages de type THEYdes malades avec un état de la requête bien reçue 
	elif request.method == 'POST': #si la requête  reçue par le serveur est un POST, l'hôpital va recevoir la liste des message dits par le client déclaré malade
		if request.is_json: #si le contenu de la requête est bien sous un format json
			req = json.loads(request.get_json()) #importe les données json envoyées par le client (str) en Python
			response = client.catalog.c_import_as_type(req, Message.MSG_THEY) #on crée une copie de chaque message de req, on déclare leur type comme THEY et on stocke le nombre de messages d'un individu malade ajoutés à la liste THEY SAID de l'hôpital
			response = Response(f"{response} new “they said” messages.", mimetype='text/plain', status=201) #la réponse informe que de nouveaux messages de malades ont été rajoutés, la requête a bien été créée.
		else:
			response = Response(f"JSON expected", mimetype='text/plain', status=400) #la réponse est une erreur précisant que les données du client doivent être en format json
	else:
		reponse = Response(f"forbidden method “{request.method}”.",status=403) #la méthode est interdite, l'accès est interdit.
	return response #renvoit la réponse
#End hospital use case

#Begin UI
#These routes are out of the scope of the project, they are here
#to provide a test interface
@app.route('/check/<host>', methods=['GET'])
def check(host):
	h = host.strip()
	n = client.get_covid(h)
	r = dict()
	r["text"] = f"{n} they said messages imported from {h} ({client.r.status_code})"
	if client.catalog.quarantine(4):
		r["summary"] = "Stay home, you are sick."
	else:
		r["summary"] = "Everything is fine, but stay home anyway."
	return render_template("menu.html",responses=[r],root=request.url_root, h=h)

@app.route('/declare/<host>', methods=['GET'])
def declare(host):
	h = host.strip()
	client.send_history(h)
	r=[{"summary":f"Declare covid to {h} ({client.r.status_code})",
	"text":client.r.text}]
	return render_template("menu.html",responses=r,root=request.url_root, h=h)

@app.route('/say/<hosts>', methods=['GET'])
def tell(hosts):
	hosts = hosts.split(",")
	r=[]
	for host in hosts:
		h = host.strip()
		client.say_something(h)
		r.append({"summary":f"ISAID to {h} ({client.r.status_code})",
		"text":client.r.text})
	return render_template("menu.html", responses=r, root=request.url_root, h=h)
#end UI

#will only execute if this file is run
if __name__ == "__main__":
	debugging = True
	client = Client("client.json", debugging)
	app.run(host="0.0.0.0", debug=debugging)
