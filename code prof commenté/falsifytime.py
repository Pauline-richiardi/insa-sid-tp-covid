#!/usr/bin/env python3
def falsifytime(): #fonction qui permet récupérer la date actuelle réelle et qui renvoit la date actuelle fictive
	import time as time
	#the referencetime is when we load this module
	referencetime = time.time() #temps de référence t=0 lors du lancement du programme
	#factor is how many seconds of fake time correspond to a real second
	factor = 3600*24*14/60 #one real minute is fourteen days of fake time

	#override time.time()
	real_time = time.time #valeur de la date actuelle (temps réel)
	def fake_time():
		duration = real_time() - referencetime #on calcule la durée écoulée entre le lancement du programme et le temps actuel
		return referencetime + factor*duration #au temps de référence on ajoute la durée écoulée en temps fictif pour retomber sur la date actuelle fictive
	time.time = fake_time #affecte à la variable temps actuel le temps fictif renvoyé par la fonction fake_time()
	return time #renvoit le temps actuel fictif.
