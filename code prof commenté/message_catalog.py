#!/usr/bin/env python3
import json, os
from message import *

class MessageCatalog:
	#constructor, loads the messages (if there are any)
	#in this implementation we store all messages in a json file
	def __init__(self, filepath): #récupère les données json d'un fichier et crée un catalogue Python
		#            I_SAID,I_HEARD,THEY
		self.data = [  []  ,  []   , [] ] #liste contenant des sous-listes avec des messages de type I_SAID,I_HEARD,THEY
		self.filepath = filepath #récupère le chemin du fichier json
		if os.path.exists(self.filepath): #si ce chemin existe
			self.open() #on ouvre le fichier
		else:
			self.save() #sinon on enregistre le fichier de données dans le chemin indiqué

	#destructor closes file.
	def __del__(self):
		del self.filepath #on ferme le chemin de la classe
		del self.data #on ferme les données de la classe

	#get catalog size
	def get_size(self, msg_type=False):
		if(msg_type is False): #si le type de message n'est pas précisé
			res = self.get_size(Message.MSG_ISAID) + self.get_size(Message.MSG_IHEARD) + self.get_size(Message.MSG_THEY) #on affiche le catalogue de tous les messages des 3 types
		else:
			res = len(self.data[msg_type]) #sinon on retourne uniquement la liste de message du type précisé
		return res

	#Import object content
	def c_import(self, content, save=True):#importe du contenu et le nombre de messages contenus
		i = 0
		for msg in content: #pour chaque message dans la série de messages du contenu que l'on souhaite importer
			#we don't save after adding message because we just
			#read the content
				if self.add_message(Message(msg), False): #si la fonction add_message est vraie, soit qu'on rajoute un nouveau message sans le sauvegarder
					i=i+1 #on incrémente
		if save:
			self.save() #on enregistre le fichier catalogue
		return i #renvoit le nombre de messages ajoutés au catalogue

	#Import a set of messages but changing their type to new_type
	def c_import_as_type(self, content, new_type=Message.MSG_THEY): #le 
		i=0
		for msg in content: #pour chaque message dans la série de messages du contenu que l'on souhaite importer
			m = Message(msg) #on crée une copie du message de la liste content
			m.set_type(new_type) #on déclare chaque message comme celui d'un malade de type THEY
			if self.add_message(m, False): #si la fonction add_message est vraie, soit qu'on rajoute un nouveau message sans le sauvegarder
				i=i+1 #on incrémente (compte le nombre de messages ajoutés)
		if i > 0: #si on a ajouté des messages
			self.save() #on enregistre le fichier
		return i #renvoit le nombre de messages d'un individu malade ajoutés au catalogue

	#Open = load from file
	def open(self):
		file = open(self.filepath,"r") #ouvre et lit le fichier
		self.c_import(json.load(file), False) #on lit et importe en Python le fichier servant de contenu sans le sauvegarder
		file.close() #on ferme le fichier

	#export the messages of a given type from a catalog
	def c_export_type(self, msg_type, indent=2, bracket=True): #exporte les messages d'un certain type en format chaine de caractères
		tmp = int(bracket) * "[\n" #on ouvre les crochets
		first_item = True #premier élément de la liste tmp
		for msg in self.data[msg_type]: #pour chaque message d'un certain type
			if first_item: #à la première itération, le premier élément est True, on rentre dans le if
				first_item=False #le premier élément de tmp passe à False, on ne rentre plus dans le if aux itérations suivantes
			else:
				tmp = tmp +",\n" #on ajoute un retour à la ligne
			tmp = tmp + (indent*" ") +  msg.m_export() #on rajoute un espace puis avec la méthode de la classe Message m_export, on convertit le message Python en chaine de caractères
		tmp = tmp + int(bracket) * "\n]" #après avoir parcouru toute la liste de messages d'un certain type, on ferme les crochets
		return tmp #renvoit la liste de messages du type demandé en chaînes de caractères, exploitables en json 

	#Export the whole catalog under a text format
	def c_export(self, indent=2): #exporte tout le catalogue en format texte
		tmp = "" #début de la chaîne de caractères
		if(len(self.data[Message.MSG_ISAID])> 0 ): #s'il y a des messages dans la liste de messages de type ISAID
			tmp = tmp + self.c_export_type(Message.MSG_ISAID, indent, False) #affecte à tmp la chaîne de caractères associée la liste de messages de type ISAID
		if(len(self.data[Message.MSG_IHEARD]) > 0): #s'il y a des messages dans la liste de messages de type IHEARD
			if tmp != "": #après avoir rajouté la première chaîne des messages ISAID (tmp est donc différent d'une str vide)
				tmp = tmp + ",\n" #on retourne à la ligne
			tmp = tmp + self.c_export_type(Message.MSG_IHEARD, indent, False) #ajoute à tmp la chaîne de caractères associée la liste de messages de type IHEARD
		if(len(self.data[Message.MSG_THEY]) > 0): #s'il y a des messages dans la liste de messages de type THEY
			if tmp != "": #si tmp est différent d'une str vide
				tmp = tmp + ",\n" #on retourne à la ligne
			tmp = tmp + self.c_export_type(Message.MSG_THEY, indent, False) #ajoute à tmp la chaîne de caractères associée la liste de messages de type ISAID
		tmp = "[" + tmp + "\n]" #on rajoute les crochets au début et à la fin
		return tmp #renvoie le texte correspondant à tout le catalogue

	#a method to convert the object to string data
	def __str__(self): #convertit le objets Python en chaîne de caractères
		tmp="<catalog>\n" #ouverture racine catalog
		tmp=tmp+"\t<isaid>\n" #balise noeud ISAID
		for msg in self.data[Message.MSG_ISAID]: #pour chaque message dans la liste de messages de type ISAID
			tmp = tmp+str(msg)+"\n" #on ajoute à tmp le message en chaîne de caractères puis on saute une ligne
		tmp=tmp+"\n\t</isaid>\n\t<iheard>\n" #fermeture noeud ISAID et ouverture noeud IHEARD
		for msg in self.data[Message.MSG_IHEARD]: #pour chaque message dans la liste de messages de type IHEARD
			tmp = tmp+str(msg)+"\n" #on ajoute à tmp le message en chaîne de caractères puis on saute une ligne
		tmp=tmp+"\n\t</iheard>\n\t<theysaid>\n" #fermture noeud IHEARD et ouverture noeud THEYSAID
		for msg in self.data[Message.MSG_THEY]: #pour chaque message dans la liste de messages de type THEY
			tmp = tmp+str(msg)+"\n" #on ajoute à tmp le message en chaîne de caractères puis on saute une ligne
		tmp=tmp+"\n\t</theysaid>\n</catalog>" #fermeture noeud THEYSAID et fermeture catalog
		return tmp

	#Save object content to file
	def save(self): #enregistre le fichier
		file = open(self.filepath,"w") #ouvre
		file.write(self.c_export()) #ecrit dans le fichier avec l'exportation du catalogue
		file.close() #ferme le fichier
		return True

	#add a Message object to the catalog
	def add_message(self, m, save=True): #ajout d'un message
		res = True
		if(self.check_msg(m.content, m.type)): #si le contenu du message à ajouter existe déjà
			print(f"{m.content} is already there") #affiche que le message existe déjà
			res = False #on ne rajoute pas le message en question
		else:
			self.data[m.type].append(m) #on rajoute le message
			if save:
				res = self.save() #on enregistre le fichier mis à jour avec le nouveau message
		return res

	#remove all messages of msg_type that are older than max_age days
	def purge(self, max_age=14, msg_type=False): #efface du catalogue les messages agés de plus de 14jours
		if(msg_type is False): #supprime du catalogue tous les messages de plus de 14 jours
			self.purge(max_age, Message.MSG_ISAID) #supprime les tous les messages de type ISAID de plus de 14 jours
			self.purge(max_age, Message.MSG_IHEARD) #supprime les tous les messages de type IHEARD de plus de 14 jours
			self.purge(max_age, Message.MSG_THEY) #supprime les tous les messages de type THEY de plus de 14 jours
		else: #si un type de message est précisé
			removable = [] #création d'une liste dans laquelle on rajoute les messages obsolètes, comme une corbeille
			for i in range(len(self.data[msg_type])): #dans la liste du type de messages concernés
				if(self.data[msg_type][i].age(True)>max_age): #si l'âge (en jours) du message parcouru est de plus de 14 jours
					removable.append(i) #on place le message dans la liste "removable", corbeille
			while len(removable) > 0: #tant que la corbeille n'est pas vide
					del self.data[msg_type][removable.pop()] #on supprime récupère l'indice du dernier élément et on supprime ce dernier élément de la corbeille
			self.save() #on enregistre le fichier mis à jour
		return True #renvoie un boléen pour dire que la purge a été effectuée

	#Check if message string is in a category
	def check_msg(self, msg_str, msg_type=Message.MSG_THEY): #prend en argument un message (chaine de caractères), et le type de messages THEY, type des malades
		for msg in self.data[msg_type]: #pour chaque message de type THEY (malades)
			if msg_str == msg.content: #teste si le contenu du message est identique à celui d'un message d'un malade
				return True #le message est celui d'un malade
		return False #le message est celui d'un individu sain

	#Say if I should quarantine based on the state of my catalog
	def quarantine(self, max_heard=4): #renvoie True si l'individu doit se confinern ou non, par exemple pour 4 contacts avec un individu contaminé
		self.purge() #on met à jour le catalogue de l'individu
		n = 0
		for msg in self.data[Message.MSG_IHEARD]: #pour chaque message dans la liste de type IHEARD du catalogue 
			if self.check_msg(msg.content): #si le message parcouru est identique à celui d'un malade
				n = n + 1 #on incrémente le compteur
		print(f"{n} covid messages heard") #affiche le nombre de messages d'individus malades entendus par un individu
		return max_heard < n #si le nombre de messages des malades que l'individu a entendus est supérieur à 4, la fonction renvoit True, l'individu doit se confiner

#will only execute if this file is run
if __name__ == "__main__":
	#test the class
	catalog = MessageCatalog("test.json") #création du catalogue de test en format json
	catalog.add_message(Message()) #ajout de message que l'individu a dit
	catalog.add_message(Message(Message.generate(), Message.MSG_IHEARD)) #ajout d'un nouveau message entendu dans le catalogue
	print(catalog.c_export()) #impression du catalogue
	time.sleep(0.5) #une demie seconde s'écoule
	catalog.add_message(Message(f"{{\"content\":\"{Message.generate()}\",\"type\":{Message.MSG_THEY}, \"date\":{time.time()}}}"))
	catalog.add_message(Message()) #ajout de message que l'individu a dit
	print(catalog.c_export()) #exporte tout le catalogue en format texte
	time.sleep(0.5) #une demie seconde s'écoule
	catalog.add_message(Message()) #ajout de message que l'individu a dit
	print(catalog.c_export()) #exporte tout le catalogue en format texte
	time.sleep(2) # 2 secondes s'écoulent
	catalog.purge(2) #purge tous les messages de plus de 2 jours
	print(catalog) #affiche le catalogue
