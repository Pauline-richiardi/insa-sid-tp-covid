#!/usr/bin/env python3
from message import Message#on importe la class message
from message_catalog import MessageCatalog#on importe la classe message catalog
import requests#on importe requests

class Client:
	#constructor, takes a message catalog
	def __init__(self, catalog_path, debug = False, defaultProtocol="http://"): #initialisation de la classe client
		self.catalog = MessageCatalog(catalog_path) #un client possède un catalogue d'appels
		if debug:
			print(self.catalog)
		self.r = None
		self.debug = debug #False par défaut signifie que l'on ne souhaite pas afficher les informations qui pourraient nous aider à débugguer en cas de problème
		self.protocol = defaultProtocol #on affecte le protocole de l'URL par défaut

	@classmethod #cette méthode peut être réalisée en dehors de l'instance Client
	def withProtocol(cls, host): #host est le serveur 
		res = host.find("://") > 1 #res stocke l'indice du premier élément trouvé dans le serveur avec
		return res #renvoie cet indice

	def completeUrl(self, host, route = ""): #permet de compléter l'URL
		if not Client.withProtocol(host): #si le client n'a pas de protocole
			host = self.protocol + host #on utilise le protocole par défaut
		if route != "": #si le chemin d'accès à la ressource est précisé
			route = "/"+route #on rajoute un "/" devant la route
		return host+route #l'URL est renvoyé sous la forme "http://nom_domaine/route"

	#send an I said message to a host
	def say_something(self, host): #informe du statut d'une requete
		m = Message() #création d'un message
		self.catalog.add_message(m) #ajout du message m
		route = self.completeUrl(host, m.content) #on complète l'URL dans route avec le contenu du message à la fin
		self.r = requests.post(route) #on poste dans le serveur associé le contenu du message
		if self.debug: #si l'on souhaite débugguer et afficher les informations explicatives
			print("POST  "+route + "→" + str(self.r.status_code)) #on affiche POST->route->l'état de la requête
			print(self.r.text) #permet d'afficher la réponse du serveur en unicode
		return self.r.status_code == 201 #renvoie True si la requête a bien été créée et False si la requête n'a pas été créée dans le serveur.

	#add to catalog all the covid from host server
	def get_covid(self, host): #permet de récupérer la liste THEY SAID à l'hôpital
		route = self.completeUrl(host,'/they-said')#on complète l'URL avec le chemin d'accès à la liste de messages THEY SAID du serveur
		self.r = requests.get(route) #requête permettant de demander la liste THEY SAID à l'hôpital
		res = self.r.status_code == 200 #renvoie True si la requête a bien été reçue et False si la requête n'a pas été reçue dans le serveur.
		if res: #si la requête à bien été reçue par le serveur
			res = self.catalog.c_import(self.r.json()) #on importe le contenu de la requête dans le catalogue en format json fonction c_import, mais on affecte res le nombre de messages THEY SAID ajoutés au catalogue
		if self.debug: #si l'on souhaite débugguer et afficher les informations explicatives
			print("GET  "+ route + "→" + str(self.r.status_code)) #on affiche GET->route->l'état de la requête
			if res != False: #si la requête a été reçue par le serveur
				print(str(self.r.json())) #on affiche la chaîne de caractères associée au contenu de la requête en format json
		return res #renvoie le nombre de messages ajoutés dans le catalogue si la connexion a été faite, sinon renvoie False

	#send to server list of I said messages
	def send_history(self, host):  #permet de déclarer le covid et informer l'hôpital
		route = self.completeUrl(host,'/they-said') #on complète l'URL avec le chemin d'accès à la liste de messages THEY SAID du serveur
		self.catalog.purge(Message.MSG_ISAID) #met à jour l'ensemble des messages que j'ai dit par rapport à la date actuelle
		data = self.catalog.c_export_type(Message.MSG_ISAID) #affecte à data l'ensemble des messages ISAID du catalogue du client
		self.r = requests.post(route, json=data) #on poste tous les messages que j'ai dit dans le serveur (hôpital)
		if self.debug:
			print("POST  "+ route + "→" + str(self.r.status_code)) #on affiche POST->route->l'état de la requête
			print(str(data)) #on affiche en chaînes de caractères l'ensemble des messages que j'ai dit
			print(str(self.r.text)) #permet d'afficher la réponse du serveur en unicode
		return self.r.status_code == 201 #renvoie True si la requête a bien été créée et False si la requête n'a pas été créée dans le serveur.


if __name__ == "__main__": #le serveur est localhost:5000"
	c = Client("client.json", True) #on crée un client dont on va afficher le catalogue
	c.say_something("localhost:5000") #envoie au serveur un nouveau message et renvoie s'il a bien été créé ou non dans le serveur, on affiche tous les éléments de "debug"
	c.get_covid("localhost:5000") #récupère la liste THEY SAID de l'hôpital (import dans le catalogue client) et renvoie le nombre de messages ajoutés si la requête a bien été reçue, on affiche tous les éléments de "debug"
	c.send_history("localhost:5000") #poste tous les messages que j'ai dit en tant que malade dans le serveur (hôpital) et renvoie True si la requête a bien été créée dans le serveur.
